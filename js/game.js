
var spielfeld = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
];

var spieler_am_zug = 1;

$(function () {
    draw(spielfeld);
    addClick();
});

function addClick() {
    $('.spielbutton').click(function () {
            var meineid = $(this).data('id');
            var y = 0;
            if (spielfeld[0][meineid] != 0) {
                return false;
            }

            while (y < 5 && spielfeld[y + 1][meineid] === 0) {
                y++;
            }

            spielfeld[y][meineid] = spieler_am_zug;
            var winner = checkIfWon(spieler_am_zug);

            switchPlayer();

            draw(spielfeld);

            if (winner != 0) {
                console.log(" Gewinner ist " + winner);

                if ((winner === 1)) {
                    console.log("Rot gewinnt")
                    $('.gewinner').append('<h1 class="text-center">Rot hat gewonnen!</h1>');
                    $("button.spielbutton ").prop("disabled",true);
                    //document.location.reload();
                }
                else if (winner === 2) {
                    console.log("Blau gewinnt");
                    $('.gewinner').append('<h1 class="text-center">Blau hat gewonnen!</h1>');
                    $("button.spielbutton ").prop("disabled",true);
                     //document.location.reload();
                }


            }
        }
    )
}

function switchPlayer() {
    switch (spieler_am_zug) {
        case 1:
            spieler_am_zug = 2;
            $("div.indicatorContainerLeft").show();
            $("div.indicatorContainerRight").hide();
            break;
        case 2:
            spieler_am_zug = 1;
            $("div.indicatorContainerLeft").hide();
            $("div.indicatorContainerRight").show();
            break;
    }
}



function draw(spielfeld) {
    $('#spielfeld').empty();
    for (var y = 0; y < 7; y++) {
        var spalte = $('<div class="spalte spalte-' + y + '"></div>');
        for (var x = 0; x < 6; x++) {
            var element = '<div class="feld reihe-' + x + ' bg-' + spielfeld[x][y] + '"></div>';
            var reihe = $(element);
            $(spalte).append(reihe);
        }
        $('#spielfeld').append(spalte);
    }
}


function checkIfWon(spieler_am_zug) {
    var steine_in_reihe = 0;
    var winner = 0;

    // vertical check
    for (var y = 0; y < 6; y++) {
        for (var x = 0; x < 7; x++) {
            if (spielfeld[y][x] === spieler_am_zug) {
                steine_in_reihe++;
            } else {
                steine_in_reihe = 0;
            }
            if (steine_in_reihe == 4) {
                winner = spieler_am_zug;
            }
        }
    }
    // horizontal check
    for (x = 0; x < 7; x++) {
        for (y = 0; y < 6; y++) {
            if (spielfeld[y][x] === spieler_am_zug) {
                steine_in_reihe++;
            } else {
                steine_in_reihe = 0;
            }
            if (steine_in_reihe == 4) {
                winner = spieler_am_zug;
            }
        }
    }

    /*
    // Diagonal check:
    for (var x = -2; x < 4; x++) {
        steine_in_reihe = 0;
        console.log('Schleifenwert x:' + x + ' ------------------');
        for (var y = 0; y <= 6; y++) {
            console.log('x:' + (x + y) + ' | y:' + y);
            if (x + y < 0) continue;
            if (x + y > 5) continue;
            if (x + y > 0) {
                console.log('spielfeld-wert:' + spielfeld[x + y][y]);
            }
            if (x + y > 0 && spielfeld[x + y][y] === spieler_am_zug) {
                steine_in_reihe++;
            } else {
                steine_in_reihe += 7;
            }
            if (steine_in_reihe == 4) {
                winner = spieler_am_zug;
            }
        }
    }
    // Diagonal check:
    for (var x = -2; x < 4; x++) {
        steine_in_reihe = 0;
        console.log('Schleifenwert x:' - x - ' ------------------');
        for (var y = 5; y <= 0; y--) {
            console.log('x:' - (x - y) - '| y:' - y);
            if (x - y < 0) continue;
            if (x - y > 5) continue;
            if (x - y > 0) {
                console.log('spielfeld-wert:' - spielfeld[x - y][y]);
            }
            if (x - y > 0 && spielfeld[x - y][y] === spieler_am_zug) {
                steine_in_reihe--;
            } else {
                steine_in_reihe -= 7;
            }
            if (steine_in_reihe == 4) {
                winner = spieler_am_zug;
            }
        }
    }
    */

    // Diagonal absteigend
    for (xStart = -2; xStart < 4; xStart++) {
        steine_in_reihe = 0;
        y = 0;
        x = xStart;

        while (x <= 6 && y <= 5) {
            if(x >= 0) {
                if( spielfeld[y][x] === spieler_am_zug ) {
                    steine_in_reihe++;
                    if (steine_in_reihe === 4) {
                        winner = spieler_am_zug;
                    }
                } else {
                    steine_in_reihe = 0;
                }
            }
            x++;
            y++;
        }
    }


    // Diagonal aufsteigend
    for (xStart = -2; xStart < 4; xStart++) {
        steine_in_reihe = 0;
        y = 5;
        x = xStart;

        while (x <= 6 && y >= 0) {
            if(x >= 0) {
                if ( spielfeld[y][x] === spieler_am_zug) {
                    steine_in_reihe++;
                    if (steine_in_reihe === 4) {
                        winner = spieler_am_zug;
                    }
                } else {
                    steine_in_reihe = 0;
                }
            }
            x++;
            y--;
        }
    }

    return winner;
}


